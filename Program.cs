﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace ProfitCenterClient
{
    class Program
    {
        //Исторические данные не нужны, достаточно значений
        static History ValueHistory = new History();
        /// <summary>
        /// Клиент, получающий значения и считающий статистику
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {// в отдельном потоке запускается считывание
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start();
                //в основном потоке будет печптаться статистика
                PrintStatistic();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {//чтобы консолька не закрылась при ошибке
                Console.ReadKey();
            }
        }
        /// <summary>
        /// Считывание сообщения
        /// </summary>
        private static void ReceiveMessage()
        {
            Clientconf config = ReadXmlFile(); //получаем параметры 

            try
            {
                while (true)
                {///пихаю создание подключения и закрытия в цикл чтобы пакеты стали теряться в связи с ожиданием
                    UdpClient receiver = new UdpClient(config.port); // UdpClient для получения данных
                    receiver.JoinMulticastGroup(config.ip);//добавляем в группу
                        IPEndPoint remoteIp = null;
                    /*      try
                          {// если не переоткрывать соединение то лаг не приводит к потере, а лишь задерживает обработку
                              while (true)
                              {*/
                    byte[] data = receiver.Receive(ref remoteIp); // получаем данные
                        ValueHistory.AddValueToDictionary(Encoding.Unicode.GetString(data));//Добавляем значение в историю
                        receiver.Close();
                    Thread.Sleep(config.lag); //задержка для потери пакетов
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Считает и выводит статистику
        /// </summary>
        private static void PrintStatistic()
        {// постоянно ждем нажатия кнопки
            while (true)
            {
                Console.ReadKey();
                History PackegeValue = (History)ValueHistory.Clone(); //сделаем клон с истории, чтобы данные дальше грузились, а я спокойно посчитал статистику
                Statistic stat = StatCalculation(PackegeValue);//Считаем все показатели
                Console.WriteLine("Среднее:{0}, Стандартное отклонение: {1}, Мода: {2}, Медиана: {3}, Принято пакетов:{4}, Потеряно пакетов:{5} ", stat.avg, stat.sko, stat.moda, stat.mediana, stat.accepted, stat.unaccepted); //выводим шаблоном
            }
        }
        /// <summary>
        /// Читает XML файл с настройками
        /// </summary>
        /// <returns></returns>
        static Clientconf ReadXmlFile()
        {
            Clientconf config = new Clientconf();
            XmlDocument xDoc = new XmlDocument();
            //файл лежит в дирректории с ехе
            xDoc.Load("setting.xml");
            //получаем корневой узел
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
            {
                //обходим узел по вложенным
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    //разбираем значения из файла в класс
                    if (childnode.Name == "ip")
                    {
                        config.ip = IPAddress.Parse(childnode.InnerText);
                    }
                    if (childnode.Name == "port")
                    {
                        config.port = Convert.ToInt32(childnode.InnerText);
                    }
                    if (childnode.Name == "lag")
                    {
                        config.lag = Convert.ToInt32(childnode.InnerText);
                    }
                }
            }
            return config;
        }
        /// <summary>
        /// Расчет статистики
        /// </summary>
        /// <param name="PackegeValue"></param>Пакет с пришедшими данными
        /// <returns></returns>//Возврашяет класс статистических параметров
        private static Statistic StatCalculation(History PackegeValue)
        {
            Statistic stat = new Statistic();// новый объект
            List<KeyValuePair<int, long>> ValueList = PackegeValue.getValueList(); //получаем лист из словаря значений
            stat.accepted = PackegeValue.AllValueCount;//принято пакетов 
            stat.unaccepted = Convert.ToInt64( PackegeValue.AllPackeges - PackegeValue.AllValueCount); //пакетов потеряно
            stat.avg = Math.Round(PackegeValue.AllValueSum / PackegeValue.AllValueCount, 2);// сразу могу посчитать среднее
            ValueList = ValueList.OrderByDescending(u => u.Value).ToList();// сортируем по количеству пришедших значений
            stat.moda = ValueList[0].Key;// мода , значение которое приходит чаше всего
            ValueList = ValueList.OrderBy(u => u.Key).ToList();// сортируем по самому значению для поиска медианы
            double mediana = PackegeValue.AllValueCount / 2;//у меня есть общее количество, могу найти место нахождения медианы в листе
            double sko = 0;// переменная под ско
            for (int i=0; i< ValueList.Count; i++)
            {//чтобы не пробегать два раза цикл ищем медиану и ско одновременно
                if(ValueList[i].Value< mediana || mediana<0)
                { //ищем медиану в листе
                    mediana -= ValueList[i].Value; }
                else
                { // чтобы выйти из условия ставлю -1, переменная будет минусоваться но медиана уже нацйдена 
                    stat.mediana = ValueList[i].Key;
                    mediana = -1;
                }//у меня уже есть среднее, считаю дисперсию
                sko = ValueList[i].Value*System.Math.Pow(ValueList[i].Key - stat.avg, 2);
            }//ско  
            stat.sko= System.Math.Round(System.Math.Sqrt(sko / PackegeValue.AllValueCount), 2);
            return stat;
        }

    }



}
