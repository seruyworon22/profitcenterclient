﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace ProfitCenterClient
{
    /// <summary>
    /// История приходящих значений
    /// </summary>
    public class History : ICloneable
    {
        //справочник значений и количества повторов этого значения
        //справочник чтобы не бегать по листу или массиву а обратиться по ключу
        Dictionary<int, long> dictionary = new Dictionary<int, long>();
        //всего значений принято
        public  double AllValueCount { get; set; }
        //сумма принятых значений
        public double AllValueSum { get; set; }
        //всего передано пакетов
        public  long AllPackeges { get; set; }
        //первый пакет который считал клиент
        private long FirstPackege { get; set; }
        //смещение в расчете приема пакетов
        private long Lag { get; set; }
        /// <summary>
        /// добавить значение в словарь
        /// </summary>
        /// <param name="message"></param>значение
        public void AddValueToDictionary(string message)
        {
            string[] mass = message.Split(';');//разбираю велосипед на значение и номер пакета
            int value = Convert.ToInt32(mass[0]);// значение идет первым в сообщении
            if (dictionary.ContainsKey(value))// если в словаре уже есть такой ключ
            { dictionary[value]++; }// плюс повтор
            else//ключа нет
            { dictionary[value] = 1; }//значение пришло первый раз
            //чтобы оптимизировать расчет статистики нужно иметь среднее, это позволит не пробегать цикл лишний раз
            AllValueCount++;//количество значений в словаре
            AllValueSum += value;// сумма всех пришедших значений
            if (FirstPackege == 0)//если нет первого пакета
                FirstPackege = Convert.ToInt64(mass[1])-1;//запоминием какой пакет клиент считал первым
            if (Convert.ToInt64(mass[1]) <= FirstPackege)//номер прищедшего меньше первого если сервак упал, его перезапустили и он начал писать пакеты с 0 
            {
                FirstPackege = Convert.ToInt64(mass[1])-1;// переписываем номер первого
                Lag = Lag + AllPackeges;//расчет принятых пойдет с нуля, а лаг учтет пакеты принятые от старого сервера 
            }
            AllPackeges = Convert.ToInt64(mass[1])+ Lag - FirstPackege;//передано пакетов
        }
        /// <summary>
        /// получить лист значений
        /// </summary>
        /// <returns></returns>//лист значений
        public List<KeyValuePair<int, long>>  getValueList()
        {
            return dictionary.ToList();//приводим к листу
        }
        /// <summary>
        /// клонирование класса
        /// </summary>
        /// <returns></returns>//клон History
        public object Clone()
        {// чтобы новые считывания не влияли на статистику
            return this.MemberwiseClone();
        }
    }
    /// <summary>
    /// Статистические показатели
    /// </summary>
    class Statistic
    {
        //среднее
        public double avg { get; set; }
        //ско
        public double sko { get; set; }
        //мода
        public int moda { get; set; }
        //медиана
        public int mediana { get; set; }
        //потеряно
        public long unaccepted { get; set; }
        //принято
        public double accepted { get; set; }
    }
    /// <summary>
    /// Клиентские настройки
    /// </summary>
    class Clientconf
    {
        //адрес для мультикаста
        public IPAddress ip { get; set; }
        //порт прослушки
        public int port { get; set; }
        //задержка в чтении
        public int lag { get; set; }
    }
}
